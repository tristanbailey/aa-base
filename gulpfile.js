var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // mix.sass('app.scss');

    mix.styles([
        // 'bootstrap/bootstrap.min.css',
        // 'flat-ui/flat-ui.css',
        'chartist/chartist.min.css',
        'bootstrap-sweetalert/lib/sweet-alert.css',
        'toastr/toastr.min.css',
    ], 'public/css/vendors.css', 'public/assets/vendors');

    mix.styles([
        '../../css/vendors.css',
        'stylesheets.css',
    ], 'public/css/all.css', 'public/assets/css/');

    mix.version('public/css/all.css'); // <link rel="stylesheet" href="{{ elixir('css/all.css') }}">

    mix.scripts([
        // 'js/jquery-2.2.4.min.js',
        'chartist/chartist.min.js',
        'toastr/toastr.min.js',
        '../js/functions.js',
    ], 'public/js/app.js', 'public/assets/vendors');

    // for after login js
    mix.scripts([
        // 'js/jquery-2.2.4.min.js',
        'chartist/chartist.min.js',
        'toastr/toastr.min.js',
        '../js/functions-auth.js',
    ], 'public/js/app-auth.js', 'public/assets/vendors');

//    mix.phpUnit('vendor/bin/phpunit', ''); // bin, command
    // this.registerWatcher("routeScanning", "tests/**/*.php");
});
